"use strict"

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable("users", {
            id: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                autoIncrement: true,
                primaryKey: true
            },
            rut: { type: Sequelize.STRING(12), allowNull: false, unique: true },
            name: { type: Sequelize.STRING(140), allowNull: false },
            email: {
                type: Sequelize.STRING(140),
                allowNull: false,
                unique: true
            },
            note: Sequelize.STRING(500),
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE
        })
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable("users")
    }
}
