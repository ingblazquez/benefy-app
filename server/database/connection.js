const Sequelize = require("sequelize")

const sequelize = new Sequelize(
    process.env.MYSQL_DATABASE,
    process.env.MYSQL_USER,
    process.env.MYSQL_USER_PASSWORD,
    {
        host: process.env.MYSQL_HOST,
        dialect: "mysql",
        operatorAliases: false
    }
)

module.exports = sequelize

global.sequelize = sequelize
