const Sequelize = require("sequelize")

module.exports = sequelize.define("users", {
    id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    rut: { type: Sequelize.STRING(12), allowNull: false, unique: true },
    name: { type: Sequelize.STRING(140), allowNull: false },
    email: { type: Sequelize.STRING(140), allowNull: false, unique: true },
    note: Sequelize.STRING(500)
})
