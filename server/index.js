const express = require("express")
const graphqlHttp = require("express-graphql")
const cors = require("cors");

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

//db connection
require("./database/connection")

//port
const PORT = process.env.PORT || 5000
const app = express(PORT)

//graphql
const graphQlSchema = require("./graphql/schema/index")
const graphQlResolvers = require("./graphql/resolvers/index")

//allow cors
app.use(cors());

app.use(
    "/graphql",
    graphqlHttp({
        schema: graphQlSchema,
        rootValue: graphQlResolvers,
        graphiql: true
    })
)




app.listen(PORT)
