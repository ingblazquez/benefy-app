const { buildSchema } = require("graphql")

module.exports = buildSchema(`
type Users {
    id: ID!
    rut: String!
    name: String!
    email: String!
    note: String
}

type RootQuery {
    users: [Users]!
    findUser(id: ID!): Users!
}

input UserInput {
    rut: String!
    name: String!
    email: String!
    note: String
}

input UserUpdate {
    id: ID!
    rut: String!
    name: String!
    email: String!
    note: String
}

type RootMutation {
    createUser(userInput: UserInput): Users
    updateUser(userInput: UserUpdate): Users
}

schema {
    query: RootQuery
    mutation: RootMutation
}
`)
