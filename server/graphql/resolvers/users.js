//users model
const Users = require("../../models/Users")

module.exports = {
    users: async () => {
        try {
            const results = await Users.findAll()
            return results
        } catch (err) {
            console.error(err)
        }
    },
    createUser: async ({ userInput }) => {
        try {
            const result = await Users.create({ ...userInput })
            const response = await result.toJSON()
            return response
        } catch (err) {
            console.log("err: ", err)
        }
    },
    findUser: async (args) => {
        try {
            const result = await Users.findOne({ where: { id: args.id } })
            return result
        } catch (err) {
            console.log("err: ", err)
        }
    },
    updateUser: async ({ userInput }) => {
        try {
            const instance = await Users.findByPk(userInput.id)
            const uptatedInstace =  await instance.update({ ...userInput })
            const response = await uptatedInstace.toJSON()
            return response

        } catch (err) {
            console.log("err: ", err)
        }
    }
}
