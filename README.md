
## BenefyApp Server
Aplicación desarrollada en Node.js. Se utilizó la capa gratuita de AWS para la implementación de un servicio de base de datos **Amazon Relational Database Service (MySQL)** para el funcionamiento de esta aplicación.

En el directorio raiz `/server`  (al mismo nivel de package.json), puede correr los siguientes comandos:

### `npm install`

Instala las dependencias de la aplicación en el directorio /node_modules.


### `npm run dev`

Inicia la aplicación en modo desarrollo, esta será accesible a través de: [http://localhost:5000](http://localhost:5000)


### Ruta para obtener registros en tabla:
A través de la ruta [http://localhost:5000/graphql](http://localhost:5000/graphql) se podrá acceder a la interfaz de prueba de GraphQL.


### Importante:
El comando `npm install` debe ser el primero en ser ejecutado para que la aplicación funcione correctamente.


## BenefyApp Client
Aplicación desarrollada en React.js. 

En el directorio raiz `/client` (al mismo nivel de package.json), puede correr los siguientes comandos:

### `npm install`

Instala las dependencias de la aplicación en el directorio /node_modules.


### `npm start`

Inicia la aplicación en modo desarrollo, esta será accesible a través de: [http://localhost:3000](http://localhost:3000)


### Rutas:
* [http://localhost:3000/users-list](http://localhost:3000/users-list) Acceso a listado de usuarios.
* [http://localhost:3000/new-user](http://localhost:3000/new-user) Acceso a formulario de creación de usuario.
* [http://localhost:3000/user-detail/:id](http://localhost:3000/user-detail/:id) Acceso a edición de usuario.

### Importante:
El comando `npm install` debe ser el primero en ser ejecutado para que la aplicación funcione correctamente.