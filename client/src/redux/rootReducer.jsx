import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import main from "./main/reducer"


const RootReducer = history => {
  return combineReducers({
    router: connectRouter(history),
    main
  });
};

export default RootReducer;
