import types from "./types";

const setAlert = (payload) => ({
  type: types.SET_ALERT,
  payload,
});

export default {
  setAlert,
};
