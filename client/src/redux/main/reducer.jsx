import types from "./types"

const INITIAL_STATE = {
    alert: { message: "Default message", type: "success", open: false }
}

const reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.SET_ALERT:
            return {
                ...state,
                alert: action.payload
            }
        default:
            return state
    }
}

export default reducer
