import ApolloClient from "apollo-boost";

const GRAPHQL_URI = "http://3.17.66.225/graphql";

const client = new ApolloClient({
  uri: GRAPHQL_URI,
});

export default client;
