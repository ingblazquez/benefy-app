import React from "react";
import { Switch, Route, Redirect } from "react-router";

import UsersForm from "./components/views/UsersForm";
import UsersList from "./components/views/UsersList";
import Layout from "./components/Layout";

const App = () => (
  <Layout>
    <Switch>
      <Route exact path="/users-list" component={UsersList} />
      <Route path="/new-user" component={UsersForm} />
      <Route path="/user-detail/:id" component={UsersForm} />
      <Redirect to="/users-list" />
    </Switch>
  </Layout>
);

export default App;
