import React from "react";
import { useSelector } from "react-redux";
import Grid from "@material-ui/core/Grid";
import ImageWidthDesc from "../utils/ImageWidthDesc";
import FeaturedImage from "../utils/FeaturedImage";
import fashionModel from "../../images/models_with_products/IMG_1429.jpg";
import detoxModel from "../../images/models_with_products/IMG_1745.jpg";
import luzesModel from "../../images/models_with_products/Chris_1.JPG";
import botulinicaModel from "../../images/models_with_products/IMG_1683.jpg";
import squareBannerUs from "../../images/banners/country/banner_square_country.jpg";
import "./dashboard.css";

// const BRAND_NAME = process.env.REACT_APP_BRAND_NAME;
const BRAND_NAME = "Ybera París";
const COMPANY_NAME = "Ybera Chile";
const Dashboard = (props) => {
  // document.title = process.env.REACT_APP_DEFAULT_COMPANY_NAME;
  document.title = COMPANY_NAME;
  const { short_description, name } = useSelector(
    ({ main }) => main.companyInfo
  );
  //  if (loading)
  //   return (
  //    <div className="skeleton-container">
  //     <Grid item xs={12}>
  //      <Skeleton variant="text" width="50%" height={60} />
  //      <Skeleton variant="rect" width="100%" height={170} />
  //      <Skeleton variant="text" width="50%" height={60} />
  //      <Skeleton variant="rect" width="100%" height={300} />
  //     </Grid>
  //    </div>
  //   );
  //  if (error) return <p>{error.message}</p>;

  return (
    <Grid item xs={12}>
      <div className="block-content">
        <ImageWidthDesc
          image={squareBannerUs}
          title={BRAND_NAME}
          subtitle={name}
          description={short_description}
          buttonHref="/us"
        />
      </div>
      <div className="block-content">
        <div className="image-grid">
          <div className="image-grid-vertical">
            <FeaturedImage
              title="Fashion Stylist"
              description="Alisado profesional"
              href="/products/fashion"
              image={fashionModel}
            />
          </div>
          <div className="image-horizontal">
            <div className="image-horizontal__img"></div>
            <FeaturedImage
              title="Detox Healthy"
              description="Profilaxis capilar"
              href="/products/detox-healthy"
              image={detoxModel}
            />
          </div>
          <div className="image-single-right">
            <FeaturedImage
              title="Luzes e Mechas"
              description="Tonos Platinados"
              href="/products/vertigo-purple"
              image={luzesModel}
            />
          </div>
          <div className="image-single-left">
            <FeaturedImage
              title="Botulínica Capilar"
              description="Efecto Botox"
              href="/products/botulinica-capilar"
              image={botulinicaModel}
            />
          </div>
        </div>
      </div>
      {/* <h2 className="text-center">
        PRODUCTOS <span className="text-secondary"> TIENDA ONLINE</span>
      </h2> */}
      {/* <div className="block-content">
        <ProductStoreHighlightFlex products={highlightedProducts} />
      </div> */}
    </Grid>
  );
};

export default Dashboard;
