import React from "react";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import "./button-light.css";

const ButtonLight = ({
  text = "Ver más",
  onClick = () => {},
  type = "",
  disabled = false,
  icon = (
    <ChevronRightIcon className="button-light__icon-svg" fontSize="large" />
  ),
}) => {
  return (
    <button
      className="button-light "
      onClick={(e) => onClick(e)}
      type={type}
      disabled={disabled}
    >
      <span className="button-light__text">{text}</span>
      <span className="button-light__icon">{icon}</span>
    </button>
  );
};

export default ButtonLight;
