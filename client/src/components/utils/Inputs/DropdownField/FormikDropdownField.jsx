import React, { useEffect } from "react";
import "react-day-picker/lib/style.css";
import { TextField as MaterialTextField } from "@material-ui/core";
import { getIn } from "formik";
import { makeStyles } from "@material-ui/core/styles";
// import "react-phone-input-2/dist/style.css";
import EditIcon from "@material-ui/icons/Edit";
import IconButton from "@material-ui/core/IconButton";
import { useSelector, useDispatch } from "react-redux";
import { handleArrayFieldsDialog } from "../../../Layout/actions";
const ROOT_URL = process.env.REACT_APP_DEV_API_URL;

const useStyles = makeStyles(theme => ({
 textField: {
  margin: theme.spacing(1),
  width: "100%"
 },
 menu: {
  width: 250
 }
}));

export default React.memo(
 ({
  field,
  form: { touched, errors, setFieldValue },
  options = [],
  defaultValue,
  dynamicField = false,
  dialog = {},
  ...props
 }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  useEffect(() => {
   if (options[0])
    if (options[0].value && dynamicField) {
     setFieldValue(field.name, options[0].value);
    }
  }, [options, dynamicField, field.name, setFieldValue]);
  useEffect(() => {
   if (field.value === "" && options[0])
    setFieldValue(field.name, options[0].value);
  }, [field.value, field.name, options, setFieldValue]);
  const defaultDialog = {
   restEndpoint: "",
   description: "",
   fieldName: "",
   fieldLabel: "",
   title: "",
   afterSubmitAction: null
  };
  const realDialog = { ...defaultDialog, ...dialog };
  const {
   restEndpoint,
   description,
   fieldName,
   fieldLabel,
   title,
   afterSubmitAction
  } = realDialog;
  const handleClick = () =>
   dispatch(
    handleArrayFieldsDialog({
     open: true,
     restAPIURL: ROOT_URL + restEndpoint,
     title,
     description,
     fieldName,
     fieldLabel,
     afterSubmitAction
    })
   );

  return (
   <MaterialTextField
    {...field}
    {...props}
    defaultValue={defaultValue}
    value={field.value ? field.value : options[0] ? options[0].value : ""}
    onBlur={e => setFieldValue(field.name, e.currentTarget.value)}
    className={classes.textField}
    error={getIn(errors, field.name)}
    helperText={getIn(errors, field.name)}
    SelectProps={{
     native: true,
     MenuProps: {
      className: classes.menu
     }
    }}
    InputProps={{
     startAdornment: (
      <IconButton
       aria-label="edit/add items"
       onClick={handleClick}
       disabled={restEndpoint === ""}
       // onMouseDown={handleMouseDownPassword}
       color="primary"
       size="small"
      >
       <EditIcon />
       {/* {values.showPassword ? <Visibility /> : <VisibilityOff />} */}
      </IconButton>
     )
    }}
    margin="normal"
    // variant={props.variant ? props.variant : "outlined"}
    disabled={props.disabled ? true : options.length > 0 ? null : true}
    fullWidth
    select
   >
    {options.map(option => (
     <option key={option.value} value={option.value}>
      {option.label}
     </option>
    ))}
   </MaterialTextField>
  );
 },
 function areEqual(prevProps, nextProps) {}
);
