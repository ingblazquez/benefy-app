import React, { useEffect } from "react"
import { TextField as MaterialTextField } from "@material-ui/core"
import { getIn } from "formik"
import { makeStyles } from "@material-ui/core/styles"
import InputAdornment from "@material-ui/core/InputAdornment"

const useStyles = makeStyles((theme) => ({
    textField: {
        margin: theme.spacing(1),
        width: "100%"
    }
}))

export default React.memo(
    ({
        field, // { name, value, onChange, onBlur }
        form: { touched, errors, setFieldValue }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
        mandatoryValue,
        defaultValue,
        startAdornment,
        endAdornment,
        parser,
        rows = null,
        ...props
    }) => {
        const classes = useStyles()
        useEffect(() => {
            if (defaultValue || defaultValue === 0) {
                setFieldValue(field.name, defaultValue)
            }
        }, [defaultValue, field.name, setFieldValue])
        useEffect(() => {
            if (mandatoryValue) {
                setFieldValue(field.name, mandatoryValue)
            }
        }, [mandatoryValue, setFieldValue, field.name])

        const onChange = (e) => {
            setFieldValue(
                field.name,
                parser ? parser(e.target.value) : e.target.value
            )
        }

        return (
            <MaterialTextField
                {...field}
                {...props}
                className={classes.textField}
                multiline={rows}
                rows={rows}
                defaultValue={defaultValue}
                error={
                    getIn(touched, field.name) && getIn(errors, field.name)
                        ? true
                        : false
                }
                helperText={
                    getIn(touched, field.name) && getIn(errors, field.name)
                }
                // variant={props.variant ? props.variant : "outlined"}
                onChange={(a, b, c) => onChange(a, b, c)}
                fullWidth
                InputProps={{
                    startAdornment: startAdornment ? (
                        <InputAdornment position="start">
                            {startAdornment}
                        </InputAdornment>
                    ) : null,
                    endAdornment: endAdornment ? (
                        <InputAdornment position="end">
                            {endAdornment}
                        </InputAdornment>
                    ) : null
                }}
            />
        )
    },
    function areEqual(prevProps, nextProps) {}
)
