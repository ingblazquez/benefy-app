import React from "react";
import {
  Checkbox as MaterialCheckbox,
  FormControlLabel
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  textField: {
    margin: theme.spacing(1),
    width: "100%"
  }
}));

export default React.memo(
  ({ field, ...props }) => {
    const classes = useStyles();
    return (
      <FormControlLabel
        {...field}
        {...props}
        className={classes.textField}
        control={
          <MaterialCheckbox
            {...field}
            {...props}
            checked={field.value ? field.value : false}
            inputProps={{
              "aria-label": "uncontrolled-checkbox"
            }}
          />
        }
      />
    );
  },
  function areEqual(prevProps, nextProps) {}
);
