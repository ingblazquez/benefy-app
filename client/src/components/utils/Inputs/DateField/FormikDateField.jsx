import React, { useEffect } from "react";
import { TextField as MaterialTextField } from "@material-ui/core";
import { getIn } from "formik";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  textField: {
    margin: theme.spacing(1),
    width: "100%"
  }
}));

export default React.memo(
  ({
    field,
    form: { touched, errors, setFieldValue },
    defaultValue,
    // variant = "outlined",
    label = "Date Picker",
    ...props
  }) => {
    useEffect(() => {
      if (defaultValue) {
        setFieldValue(field.name, defaultValue);
      }
    }, [defaultValue, setFieldValue, field.name]);
    const classes = useStyles();
    return (
      <MaterialTextField
        {...field}
        {...props}
        id={field.name}
        label={label}
        // variant={variant}
        type="date"
        error={
          getIn(touched, field.name) && getIn(errors, field.name) ? true : false
        }
        helperText={getIn(touched, field.name) && getIn(errors, field.name)}
        className={classes.textField}
        InputLabelProps={{
          shrink: true
        }}
        fullWidth
      />
    );
  },
  function areEqual(prevProps, nextProps) {}
);
