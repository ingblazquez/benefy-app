import React, { useState } from "react";
import { NavLink, useLocation } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import MenuIcon from "@material-ui/icons/Menu";
import LocalMallOutlinedIcon from "@material-ui/icons/LocalMallOutlined";
import "./header.css";

export default function Header(props) {
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);

  const location = useLocation();
  return (
    <Grid container>
      <div className="header__navigation-container">
        <Grid item xs={12} className="header__navigation">
          <ul className="header__navigation-links">
            <li>
              <NavLink
                to="/users-list"
                className="link"
                activeClassName="link-active"
              >
                Lista de Usuarios
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/new-user"
                className="link"
                activeClassName="link-active"
              >
                Nuevo Usuario
              </NavLink>
            </li>
          </ul>
        </Grid>
      </div>
    </Grid>
  );
}
