import React from "react";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Header from "./Header";

import Alert from "../utils/Alert";
import "./layout.css";

const Layout = ({ children }) => {
  return (
    <Container maxWidth={false} className="grid-cotainer" disableGutters>
      <Header />
      <Container maxWidth="lg">
        <Grid container>{children}</Grid>
      </Container>
      <Alert />
    </Container>
  );
};

export default Layout;
