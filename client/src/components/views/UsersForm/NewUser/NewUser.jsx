import React, { Fragment } from "react"
import { useDispatch } from "react-redux"
import { useHistory } from "react-router-dom"
import { useMutation } from "@apollo/react-hooks"
import gql from "graphql-tag"
import actions from "../../../../redux/main/actions"
import Form from "../Form"

const NewUser = ({ initialValues }) => {
    const dispatch = useDispatch()
    const history = useHistory()
    const ADD_USER = gql`
        mutation createUser(
            $rut: String!
            $name: String!
            $email: String!
            $note: String!
        ) {
            createUser(
                userInput: {
                    rut: $rut
                    name: $name
                    email: $email
                    note: $note
                }
            ) {
                id
                rut
                name
                email
            }
        }
    `
    const [createUser, { data }] = useMutation(ADD_USER)
    const onSubmitForm = (values) => {
        createUser({ variables: { ...values } })
            .then(({ data }) => {
                const { createUser } = data
                const { name } = createUser
                dispatch(
                    actions.setAlert({
                        message: `Usuario ${name} creado!`,
                        type: "success",
                        open: true
                    })
                )
                history.push("/users-list")
            })
            .catch((err) => {
                dispatch(
                    actions.setAlert({
                        message: `Hubo un error, intente nuevamente...`,
                        type: "error",
                        open: true
                    })
                )
            })
    }

    return (
        <Fragment>
            <h2 className="block-content text-center">Nuevo usuario</h2>
            <Form initialValues={initialValues} onSubmitForm={onSubmitForm} />
        </Fragment>
    )
}

export default NewUser
