import * as yup from "yup"
import field from "./fieldConstants"
import { validate as validateRut } from "rut.js"

const REQUIRED_MESSAGE = "Este campo es requerido"
const INVALID_EMAIL = "Este correo es inválido"
const INVALID_RUT = "Este rut es inválido"

export default yup.object().shape({
    [field.rut.name]: yup
        .string()
        .test([field.rut.name], INVALID_RUT, (value) => validateRut(value))
        .required(REQUIRED_MESSAGE),
    [field.name.name]: yup.string().required(REQUIRED_MESSAGE),
    [field.email.name]: yup
        .string()
        .email(INVALID_EMAIL)
        .required(REQUIRED_MESSAGE)
})
