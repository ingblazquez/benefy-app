import { format as formatRut } from "rut.js"

const initialValues = {
    rut: {
        name: "rut",
        label: "Rut",
        maxLength: 12,
        required: true,
        parser: formatRut
    },
    name: {
        name: "name",
        label: "Nombre",
        maxLength: 140,
        required: true
    },
    email: {
        name: "email",
        label: "Email",
        maxLength: 140,
        required: true
    },
    note: {
        name: "note",
        label: "Nota adicional",
        rows: 4,
        maxLength: 500
    }
}

export default initialValues
