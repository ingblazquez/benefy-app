import React from "react"
import { Formik, Field } from "formik"
import TextField from "../../../utils/Inputs/TextField"
import field from "./fieldConstants"
import validationSchema from "./validationSchema"
import ButtonLight from "../../../utils/ButtonLight"

const Form = ({ initialValues, onSubmitForm }) => {
    return (
        <Formik
            enableReinitialize
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={(values) => {
                try {
                    onSubmitForm(values)
                } catch (err) {
                    console.log("err: ", err)
                }
            }}
        >
            {({ submitting, dirty, handleSubmit }) => {
                return (
                    <form onSubmit={handleSubmit}>
                        <div className="block-content">
                            <div className="form-field">
                                <Field component={TextField} {...field.rut} />
                            </div>
                            <div className="form-field">
                                <Field component={TextField} {...field.name} />
                            </div>
                            <div className="form-field">
                                <Field component={TextField} {...field.email} />
                            </div>
                            <div className="form-field">
                                <Field component={TextField} {...field.note} />
                            </div>
                        </div>
                        <div className="form-field">
                            <ButtonLight
                                text="enviar"
                                type="submit"
                                disabled={!dirty || submitting}
                            >
                                Enviar
                            </ButtonLight>
                        </div>
                    </form>
                )
            }}
        </Formik>
    )
}

export default Form
