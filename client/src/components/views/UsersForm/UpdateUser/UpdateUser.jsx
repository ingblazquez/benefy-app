import React, { Fragment } from "react"
import { useDispatch } from "react-redux"
import { useHistory } from "react-router-dom"
import { useMutation, useQuery } from "@apollo/react-hooks"
import gql from "graphql-tag"
import actions from "../../../../redux/main/actions"
import Form from "../Form"

const UpdateUser = ({ id, initialValues }) => {
    const dispatch = useDispatch()
    const history = useHistory()
    const FIND_USER = gql`
        query findUser($id: ID!) {
            findUser(id: $id) {
                id
                name
                rut
                email
                note
            }
        }
    `
    const UPDATE_USER = gql`
        mutation updateUser(
            $id: ID!
            $rut: String!
            $name: String!
            $email: String!
            $note: String!
        ) {
            updateUser(
                userInput: {
                    id: $id
                    rut: $rut
                    name: $name
                    email: $email
                    note: $note
                }
            ) {
                id
                rut
                name
                email
                note
            }
        }
    `

    const { data } = useQuery(FIND_USER, {
        variables: { id }
    })

    const [updateUser, mutatedData] = useMutation(UPDATE_USER)

    const onSubmitForm = (values) => {
        updateUser({
            variables: { ...values }
        })
            .then(({ data }) => {
                const { updateUser } = data
                const { name } = updateUser
                dispatch(
                    actions.setAlert({
                        message: `Usuario ${name} actualizado!`,
                        type: "success",
                        open: true
                    })
                )
                history.push("/users-list")
            })
            .catch((err) => {
                dispatch(
                    actions.setAlert({
                        message: `Hubo un error, intente nuevamente...`,
                        type: "error",
                        open: true
                    })
                )
            })
    }
    const detailInitialValues = data
        ? { ...data.findUser }
        : { ...initialValues }
    return (
        <Fragment>
            <h2 className="block-content text-center">Detalle de usuario</h2>
            <Form
                initialValues={detailInitialValues}
                onSubmitForm={onSubmitForm}
            />
        </Fragment>
    )
}

export default UpdateUser
