import React, { Fragment } from "react"
import { Formik, Field } from "formik"

import Grid from "@material-ui/core/Grid"
import { useParams } from "react-router-dom"
import UpdateUser from "./UpdateUser"
import NewUser from "./NewUser"

import "./users-form.css"

let initialValues = { name: "", email: "", note: "", rut: "" }

const UsersForm = (props) => {
    document.title = `Detalle de Usuario`
    let { id } = useParams()
    return (
        <Grid item xs={12}>
            <div className="block-content">
                {id ? (
                    <UpdateUser id={id} initialValues={initialValues} />
                ) : (
                    <NewUser initialValues={initialValues} />
                )}
            </div>
        </Grid>
    )
}

export default UsersForm
