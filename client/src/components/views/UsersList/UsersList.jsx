import React from "react";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import gql from "graphql-tag";
import { useQuery } from "@apollo/react-hooks";
import { useHistory } from "react-router-dom";
import "./users-form.css";

const GET_USERS = gql`
  query {
    users {
      id
      name
      rut
      email
    }
  }
`;
const UsersForm = (props) => {
  document.title = `Listado de Usuarios`;
  let history = useHistory();
  const { loading, error, data } = useQuery(GET_USERS);
  console.error(error);
  const handleClickRow = (id) => history.push(`/user-detail/${id}`);
  return (
    <Grid item xs={12}>
      <div className="block-content">
        <h2 className="block-content text-center">Listado de Usuarios</h2>
        <div className="block-content">
          {!loading ? (
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Rut</TableCell>
                  <TableCell>Nombre</TableCell>
                  <TableCell>Email</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.users.map(({ id, rut, name, email }) => {
                  return (
                    <TableRow hover onClick={() => handleClickRow(id)} key={id}>
                      <TableCell component="th" scope="row">
                        {rut}
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {name}
                      </TableCell>
                      <TableCell component="th" scope="row">
                        {email}
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          ) : (
            <p>Cargando...</p>
          )}
        </div>
      </div>
    </Grid>
  );
};

export default UsersForm;
